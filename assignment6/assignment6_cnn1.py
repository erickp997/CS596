#!/usr/bin/env python
# coding: utf-8

# # CS596 Machine Learning 
# # Assignment 5 (Part 1): Convolutional Neural Network
# 
# ### Due 11:59 pm, Friday, 11/22/2019
# 
# **Total points: 6**
# 
# In Part 1 of Assignment 6, you will first implement the following operations with numpy:
# - Zero padding
# - Single position convolution
# - Forward pass for convolution layer
# - Forward pass for pooling layer

# In[1]:


import numpy as np

np.random.seed(1)


# ## Task 6.1 Zero padding
# **1 point**
# 
# Apply padding around the border of an input image. 
# 
# A batch of inpurt images is represented by a 4-D array of shape (m, n_H, n_W, n_C). You need to pad the 2nd and 3rd dimensions (height and width) with zeros. Use the __[np.pad](https://docs.scipy.org/doc/numpy-1.15.1/reference/generated/numpy.pad.html)__ function for convenience. For example, if you want to pad the input array "a" of shape (7,7,7,7,7) with pad_size = 2 for the 2nd dimension and pad_size = 1 for the 4th dimension, with pad_values = 0, then you can write:
# 
# ```python
# a_pad = np.pad(a, ((0,0), (2,2), (0,0), (1,1), (0,0)), 'constant', constant_value=0)
# ```

# In[6]:


def zero_pad(X, pad):
    """
    Args:
    X -- a numpy array of shape (m, n_H, n_W, n_C) representing a batch of images
    pad -- an integer indicating the size of padding
    
    Returns:
    X_pad -- a padded array of shape (m, n_H + 2*pad, n_W + 2*pad, n_C)
    """
    ### START YOUR CODE ###
    X_pad = np.pad(X, ((0,0),(pad, pad),(pad,pad),(0,0)), 'constant', constant_values = 0)
    ### END YOUR CODE ###
    
    return X_pad


# In[7]:


# Evaluate Task 6.1
np.random.seed(2)
x = np.random.randn(4, 2, 2, 2)
x_pad = zero_pad(x, 1)
print ("x.shape =", x.shape)
print ("x_pad.shape =", x_pad.shape)
print ("x[0,:,:,0] =", x[0,:,:,0])
print ("x_pad[0,:,:,0] =", x_pad[0,:,:,0])


# ### Expected output
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**x.shape =**|(4, 2, 2, 2)|
# |**x_pad.shape =**|(4, 4, 4, 2)|
# |**x[0,:,:,0] =**|[[-0.41675785 -2.1361961 ]<br>[-1.79343559  0.50288142]]|
# |**x_pad[0,:,:,0] =**|[[ 0.          0.          0.          0.        ]<br>[ 0.         -0.41675785 -2.1361961   0.        ]<br>[ 0.         -1.79343559  0.50288142  0.        ]<br>[ 0.          0.          0.          0.        ]]|
# 
# ***
# 
# ## Task 6.2 Single position convolution
# **1 point**
# 
# Implement convolution on a single position of input data, i.e., applying a filter on a slice of input data (same shape as filter). A filter is represented by $W$ the weight matrix of shape (f, f, n_C_prev), and $b$ the bias parameter of shape $(1,1,1)$. You compute the element-wise product between the filter and input, and sum up the result.
# 
# *Hint:* use `np.multiply` and `np.sum`

# In[29]:


def conv_single_pos(a_slice_prev, W, b):
    """
    Args:
    a_slice_prev -- a slice of the input data, shape = (f, f, n_C_prev)
    W -- Weight parameters of the filter, shape = (f, f, n_C_prev)
    b -- Bias paramter of the filter, shape = (1, 1, 1)
    
    Returns:
    Z -- A scalar value, result of convlution.
    """
    ### START TODO ###
    # Compute the element-wise product between a_slice_prev and W
    s = np.multiply(a_slice_prev, W)
    # Sum over all entries in volume s
    Z = np.sum(s)
    # Add bias to Z. Cast b to a float() so that Z results in a scalar value.
    Z = Z + float(b)
    ### END TODO ###
    
    return Z


# In[30]:


# Evaluate Task 6.2
np.random.seed(1)
a_slice_prev = np.random.randn(4, 4, 3)
W = np.random.randn(4, 4, 3)
b = np.random.randn(1, 1, 1)

Z = conv_single_pos(a_slice_prev, W, b)
print("Z =", Z)


# ### Expected output
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**Z =**|-6.999089450680221|
# 
# ***
# 
# ## Task 6.3 Forward pass for convolution layer
# **2 points**
# 
# In the forward pass of a convolution layer, you take several filters and convolve them on the input. The convolution of each filter outputs a 2-D matrix. You stack these matrices and get the final output, a 3-D volume. Here, input is the activation from the previous layer `A_prev`. You use a moving window to select a slice from `A_prev`, and then convolve the slice with the filter denoted by `W` (weight parameters) and `b` (bias parameters).
# 
# For example, to select a $2\times 2$ slice from `a_prev`, a volume of shape (5, 5, 3), what you can do is:
# ```python
# a_prev_slice = a_prev[0:2, 0:2, 0]
# ```
# 
# To select a slice at an arbitrary position, you need to use the indices of its four corners: `vert_start`, `vert_end`, `horiz_start` and `horiz_end`. In the example above, `vert_start = horiz_start = 0`, and `vert_end = horiz_end = 2`. In any other specific cases, the values of these four corner indices are determined by filter size `f`, stride size `s`, and the position of the corresponding output unit.
# 
# **Instructions**
# - Weight parameters `W` is of shape (f, f, n_C_prev, n_C). n_C_prev is the number of channels in the previous layer, and n_C is the number of filters.
# - Bias parameter `b` is of shape (1, 1, 1, n_C).
# - Hyperparamters like `stride` and `pad` are stored in the Python dictionary object `hparams`.
# - Output is of shape (m, n_H, n_W, n_C). n_H and n_W are computed by:
#     - $n_H = \lfloor\frac{n_{H\ prev} - f + 2\times p}{s}\rfloor + 1$
#     - $n_W = \lfloor\frac{n_{W\ prev} - f + 2\times p}{s}\rfloor + 1$
#     - Each entry in the output volume is computed by calling `conv_single_pos`
# - The input volume `A_prev` has 4 dimensions, (m, n_H_prev, n_W_prev, n_C_prev), in which m is the number of training examples.

# In[39]:


def conv_forward(A_prev, W, b, hparams):
    """
    Args:
    A_prev -- Activations from the previous layer, a numpy array of shape (m, n_H_prev, n_W_prev, n_C_prev)
    W -- Weight parameters, a numpy array of shape (f, f, n_C_prev, n_C)
    b -- Bias parameters, a numpy array of shape (1, 1, 1, n_C)
    hparams -- A Python dict object containing 'stride' and 'pad'
    
    Returns:
    Z -- Output of convolution, a numpy array of shape (m, n_H, n_W, n_C)
    cache -- cache of variables needed for backward propagation
    """
    ### START YOUR CODE ###
    # Retrieve dimensions of input (A_prev)
    (m, n_H_prev, n_W_prev, n_C_prev) = A_prev.shape
    
    # Retrieve dimensions of filter (W)
    (f, f, n_C_prev, n_C) = W.shape
    
    # Retrieve hyperparameters
    stride = hparams['stride']
    pad = hparams['pad']

    # Compute n_H and n_W. Hint: use int() to floor
    n_H = int((n_H_prev - f + (2 * pad)) / stride + 1)
    n_W = int((n_W_prev - f + (2 * pad)) / stride + 1)
    
    # Initialize the output volume Z with zeros (into the right shape).
    Z = np.zeros((m, n_H, n_W, n_C))
    
    # Create A_prev_pad by padding A_prev. Hint: call zero_pad()
    A_prev_pad = zero_pad(A_prev, pad)
    
    for i in range(m):                       # Loop over all training examples
        # Select the padded activation for the ith training example
        a_prev_pad = A_prev[i]
        
        for h in range(n_H):                 # Loop over vertical axis of the output volume
            for w in range(n_W):             # Loop over horizontal axis of the output volume
                for c in range(n_C):         # Lopp over Channels of the output volume
                    
                    # Find the corner indices of the current slice
                    vert_start = stride * h
                    vert_end = stride * h + f
                    horiz_start = stride * w
                    horiz_end = stride * w + f
                    
                    # Use the corner indices defined above to find a slice from a_prev_pad
                    a_prev_slice = A_prev_pad[i, vert_start:vert_end, horiz_start:horiz_end, :]
                    
                    # Convolve the slice with the filter (W and b). Hint: call conv_single_pos()
                    Z[i, h, w, c] = conv_single_pos(a_prev_slice, W[:,:,:,c], b[:,:,:,c])
                    
    ### END YOUR CODE ###
    
    assert(Z.shape == (m, n_H, n_W, n_C))
    
    # Save to cache
    cache = (A_prev, W, b, hparams)
    
    return Z, cache


# In[40]:


# Evaluate Task 6.3
np.random.seed(1)
A_prev = np.random.randn(10,4,4,3)
W = np.random.randn(2,2,3,8)
b = np.random.randn(1,1,1,8)
hparams = {"stride": 2, "pad" : 2}

Z, cache = conv_forward(A_prev, W, b, hparams)
print("Mean of Z =", np.mean(Z))
print("Z[3,2,1] =", Z[3,2,1])
print("cache_conv[0][1][2][3] =", cache[0][1][2][3])


# ### Expected output
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**Mean of Z =**|0.048995203528855794|
# |**Z[3,2,1] =**|[-0.61490741 -6.7439236  -2.55153897  1.75698377  3.56208902  0.53036437  5.18531798  8.75898442]|
# |**cache[0][1][2][3] =**|[-0.20075807  0.18656139  0.41005165]|
# 
# ***
# 
# ## Task 6.4 Forward pass for pooling layer
# **2 points**
# 
# Max pooling layer slides a window over the input and stores the max value within the window to the output. Average pooling layer computes the average value of the window and stores it to the output.
# 
# **Instructions:**
# - Implement the two types of pooling in one function. The type (max or average) of pooling is decided by the argument `mode`
# - For max pooling, use `np.max()`; for average pooling, use `np.mean()`.
# - Pooling layer applies no padding, therefore
#     - $n_H = \lfloor\frac{n_{H\ prev} - f}{s}\rfloor + 1$
#     - $n_W = \lfloor\frac{n_{W\ prev} - f}{s}\rfloor + 1$
# - Pooling applies to each input channel independently, i.e., $n_C = n_{C_{prev}}$

# In[41]:


def pool_forward(A_prev, hparams, mode='max'):
    """
    Args:
    A_prev -- Activations from the previous layer, a numpy array of shape (m, n_H_prev, n_W_prev, n_C_prev)
    hparams -- A Python dict object containing 'stride' and 'pad'
    mode -- Pooling type, a string ('max' or 'average')
    
    Returns:
    A -- Output, a numpy array of shape (m, n_H, n_W, n_C)
    cache -- Cached variables that will be used for backward propagation 
    """
    ### START YOUR CODE ###
    # Retrieve dimensions of input (A_prev)
    (m, n_H_prev, n_W_prev, n_C_prev) = A_prev.shape
    
    # Retrieve hyperparameters
    f = hparams['f']
    stride = hparams['stride']
    
    # Compute n_H, n_W and n_C
    n_H = int(1 + (n_H_prev - f) / stride)
    n_W = int(1 + (n_W_prev - f) / stride)
    n_C = n_C_prev
    
    # Initialize output volume to zeros
    A = np.zeros((m, n_H, n_W, n_C))
    
    for i in range(m):                       # Loop over all training examples
        for h in range(n_H):                 # Loop over vertical axis of the output volume
            for w in range(n_W):             # Loop over horizontal axis of the output volume
                for c in range(n_C):         # Lopp over Channels of the output volume
                    
                    # Find the corner indices of the current slice
                    vert_start = stride * w
                    vert_end = stride * w + f
                    horiz_start = stride * h
                    horiz_end = stride * h + f
                    
                    # Use the corner indices defined above to find a slice from A_prev
                    a_prev_slice = A_prev[i, vert_start:vert_end, horiz_start:horiz_end, c]
                    
                    # Conduct pooling operation
                    if mode == "max":
                        A[i, h, w, c] = np.max(a_prev_slice)
                    elif mode == "average":
                        A[i, h, w, c] = np.mean(a_prev_slice)
                    
    ### END YOUR CODE ###
    
    assert(A.shape == (m, n_H, n_W, n_C))
    
    # Save to cache
    cache = (A_prev, hparams)
    
    return A, cache


# In[42]:


# Evaluate Task 6.4
np.random.seed(1)
A_prev = np.random.randn(2, 4, 4, 3)
hparams = {"stride" : 2, "f": 3}

A, cache = pool_forward(A_prev, hparams)
print("Max pooling = ", A)
print()
A, cache = pool_forward(A_prev, hparams, mode = "average")
print("Average pooling = ", A)


# ### Expected output
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**Max pooling =**|[[[[1.74481176 0.86540763 1.13376944]]]<br>[[[1.13162939 1.51981682 2.18557541]]]]|
# |**Average pooling =**|[[[[ 0.02105773 -0.20328806 -0.40389855]]]<br>[[[-0.22154621  0.51716526  0.48155844]]]]|
# 
# ***
# 
# ## Congratulations!
# Now you understand how the forward pass of convolution layer and pooling layer work.
