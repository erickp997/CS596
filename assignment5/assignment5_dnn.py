#!/usr/bin/env python
# coding: utf-8

# # CS596 Machine Learning 
# # Assignment 5: Deep Neural Network
# 
# ### Due 11:59 pm, Friday, 11/1/2019
# 
# **Total points: 10**
# 
# In Assignment 5, you will implement a deep neural network model. 

# In[1]:


import numpy as np
import matplotlib.pyplot as plt

import importlib
import utils
importlib.reload(utils)

get_ipython().run_line_magic('matplotlib', 'inline')
np.random.seed(1)


# ## Building blocks of DNN
# 
# The basic building unit of DNN is described in the following figure.
# 
# <img src="dnn_building_block.png" style="width: 360px"/>
# 
# Combing multiple units together, the computation graph of a DNN will be as follows:
# 
# <img src="dnn_building_block2.png" style="width: 1000px" />
# 
# ***
# 
# ## Decomposing forward block
# 
# The `forward` block of layer $l$ can be decomposed into two sub-modules: `linear_forward` and `activation_forward`
# - `linear_forward` computes $Z^{[l]} = W^{[l]}A^{[l-1]}+b^{[l]}$, and then caches $A^{[l-1]}$ into `linear_cache`. For convenience, `linear_cache` also stores $W^{[l]}$ and $b^{[l]}$.
# - `activation_forward` computes $A^{[l]} = g^{[l]}(Z^{[l]})$, and then caches $Z^{[l]}$ into `activation_cache`
# - The `forward` unit outputs $A^{[l]}$, and combines `linear_cache` and `activation_cache` into `cache`
# 
# <img src="forward_decompose.png" style="width: 800px" />
# 
# ***
# 
# ## 1 Implement linear_forward
# **0.5 point**

# In[2]:


def linear_forward(A_prev, W, b):
    """
    Args:
    A_prev -- activations from previous layer, of shape (size of previous layer, m)
    W -- weight matrix of shape (size of current layer, size of previous layer)
    b -- bias vector of shape (size of current layer, 1)
    
    Returns:
    Z -- output of the linear forward computation, and the input fed into the next activation forward module
    cache -- a python dict object containing all cached variables, i.e., A_prev, W, and b
    """
    ### START YOUR CODE ###
    Z = np.dot(W, A_prev) + b
    ### END YOUR CODE ###
    
    linear_cache = (A_prev, W, b)
    
    return Z, linear_cache


# In[3]:


# Evaluate Task 1
A, W, b = utils.linear_forward_testcase()

Z, _ = linear_forward(A, W, b)
print('Z =', Z)


# **Expected output**
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**Z =**|[[ 0.04347969 -0.01180377]<br>[-0.04910105 -0.00973779]<br>[ 0.0140403  -0.02289789]<br>[-0.01817968 -0.00986889]<br>[ 0.01867231  0.00796029]]|
# 
# ***
# 
# ## 2 Implement activation_forward
# **2 points**
# 
# Instructions:
# - The `func` argument of `activation_forward` denotes which activation function to use. You need to implement `sigmoid_forward` and `relu_forward` respectively, and then call them in `activation_forward`.
# - `sigmoid_forward` is a bit different from the pre-defined `sigmoid` function in `utils`, because you need to cache $Z$ and return it together with the activation
# - $ReLU(Z)=\begin{cases}Z, & \text{if } Z>0.\\ 0, & \text{otherwise.} \end{cases}$  You can implement it using `np.maximum` function
# - For either activation function, `cache` is just its input $Z$.

# In[4]:


def sigmoid_forward(Z):
    """
    Args:
    Z -- a numpy array of any shape
    
    Returns:
    A -- output of sigmoid(Z), same shape as Z
    cache -- Z
    """
    ### START YOUR CODE ###
    A = utils.sigmoid(Z)
    activation_cache = Z
    ### END YOUR CODE ###
    
    return A, activation_cache

def relu_forward(Z):
    """
    Args:
    Z -- a numpy array of any shape
    
    Returns:
    A -- output of sigmoid(Z), same shape as Z
    cache -- Z
    """
    ### START YOUR CODE ###
    A = np.maximum(0, Z)
    activation_cache = Z
    ### END YOUR CODE ###
    
    return A, activation_cache

def activation_forward(Z, func):
    """
    Args:
    Z -- a numpy array of any shape
    func -- a string of either 'sigmoid' or 'relu'
    
    Returns:
    A -- output of the activation function
    cache -- cache returned by the activation function
    """
    assert func in ['sigmoid', 'relu']
    
    if func == 'sigmoid': 
        ### START YOUR CODE ###
        A, activation_cache = sigmoid_forward(Z) # Call sigmoid_forward
        ### END YOUR CODE ###
    elif func == 'relu':
        ### START YOUR CODE ###
        A, activation_cache = relu_forward(Z) # Call sigmoid_forward
        ### END YOUR CODE ###
    
    return A, activation_cache


# In[5]:


# Evaluate Task 2
Z = utils.activation_forwad_testcase()

A, _ = activation_forward(Z, func='sigmoid')
print('With sigmoid function, A =', A)
A, _ = activation_forward(Z, func='relu')
print('With sigmoid function, A =', A)


# **Expected output**
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**With sigmoid function, A =**|[0.73105858 0.26894142 0.5       ]|
# |**With sigmoid function, A =**|[1 0 0]|
# 
# ***
# 
# ## 3 Implement forward
# **0.5 point**
# 
# In `forward`, call `linear_forward` and `activation_forward` in order, and return the activation value and the combined cache.

# In[6]:


def forward(A_prev, W, b, func):
    """
    Args:
    A_prev -- activation from previous layer
    W -- Weight matrix
    b -- bias vector
    func -- 'sigmoid' or 'relu'
    
    Returns:
    A -- activation
    cache -- tuple of (linear_cache, activation_cache)
    """
    # Compute output of linear_forward and activation_forward
    ### START YOUR CODE ###
    Z, linear_cache = linear_forward(A_prev, W, b)
    A, activation_cache = activation_forward(Z, func)
    ### END YOUR CODE ###
        
    cache = (linear_cache, activation_cache)
    
    return A, cache


# In[7]:


# Evaluate Task 3
A_prev, W, b = utils.forward_testcase()

A, _ = forward(A_prev, W, b, func='sigmoid')
print('With sigmoid function, A =', A)
A, _ = forward(A_prev, W, b, func='relu')
print('With relu function, A =', A)


# **Expected output**
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**With sigmoid function, A =**|[[0.51086821 0.49704909]<br>[0.4877272  0.49756557]<br>[0.50351002 0.49427578]<br>[0.4954552  0.4975328 ]<br>[0.50466794 0.50199006]]|
# |**With sigmoid function, A =**|[[0.04347969 0.        ]<br>[0.         0.        ]<br>[0.0140403  0.        ]<br>[0.         0.        ]<br>[0.01867231 0.00796029]]|
# 
# ***
# 
# ## 4 Initialize parameters for DNN
# **1 point**
# 
# The number of layers and layer sizes are specified by a Python list `layer_sizes`. For example, if `layer_sizes = [4096, 128, 64, 10, 1]`, then the model has **4** layers (The first element 4096 is the size of input, which is not counted as a layer).
# `layer_sizes[1]` is the number of hidden units in layer 1, `layer_sizes[2]` is for layer 2, and so on.
# 
# Therfore, the shapes of parameters associated with each layer are: 
# - $W^{[1]}: (128, 4096)\quad b^{[1]}: (128,1)$
# - $W^{[2]}: (64, 128)\quad b^{[2]}: (64,1)$
# - $W^{[3]}: (10, 64)\quad b^{[3]}: (10,1)$
# - $W^{[4]}: (1, 10)\quad b^{[4]}: (1,1)$
# 
# In general, if the length of `layer_sizes` is $L$, then `layer_sizes[0]` is the input size, and `layer_sizes[1]` through `layer_sizes[L-2]` indicate the sizes of hidden layers. `layer_sizes[L-1]` is the size of output layer.
# 
# 
# **Instructions:**
# - Because all the hidden layers use ReLU activation, the proper way to initialize their weights is to use:<br>
# $W^{[l]}=\text{np.random.randn}(n^{[l]}, n^{[l-1]})*\text{np.sqrt}(\frac{2}{n^{[l-1]}})$, where $l=1,2,\dots,L-2$
# 
# - For the output layer, the proper way to initialize its weight is:<br>
# $W^{[L-1]}=\text{np.random.randn}(n^{[L-1]}, n^{[L-2]})*\text{np.sqrt}(\frac{1}{n^{[L-2]}})$
# 
# - All bias vectors $b^{[l]}$ are initialized to 0s.
# 
# - Parameters are stored in a python dict object, whose keys are strings like `'W1', 'b1', 'W2', 'b2'` etc.

# In[8]:


def init_params_deep(layer_sizes):
    """
    Args:
    layer_sizes -- a python list containing the sizes of input layer, hidden layers, and output layer.
    
    Returns:
    params -- a python dict object containing all initialized parameters, i.e., 'W1', 'b1', 'W2', 'b2' etc.
    """
    np.random.seed(3)
    params = {}
    L = len(layer_sizes)
    assert(L >= 2)
    
    # Initialize all L-2 hidden layers
    for l in range(1, L-1):
        ### START YOUR CODE ###
        params['W' + str(l)] = np.random.randn(layer_sizes[l],layer_sizes[l - 1]) * np.sqrt(2 / layer_sizes[l - 1])
        params['b' + str(l)] = np.zeros((layer_sizes[l], 1))
        ### END YOUR CODE ###
    
    # Initialize output layer
    ### START YOUR CODE ###
    params['W' + str(L-1)] = np.random.randn(layer_sizes[L - 1], layer_sizes[L - 2]) * np.sqrt(1 / layer_sizes[L - 2])
    params['b' + str(L-1)] = np.zeros((layer_sizes[L - 1], 1))
    ### END YOUR CODE ###
    
    return params


# In[9]:


# Evaluate Task 4
params = init_params_deep([5,3,1])

print('W1 =', params['W1'])
print('b1 =', params['b1'])
print('W2 =', params['W2'])
print('b2 =', params['b2'])


# **Expected output**
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**W1 =**|[[ 1.13122797  0.27607307  0.06103036 -1.17857627 -0.1754357 ]<br>[-0.22436928 -0.05233031 -0.39655005 -0.02771304 -0.30181918]<br>[-0.83096103  0.55948432  0.55739447  1.08122894  0.03164405]]|
# |**b1 =**|[[0.]<br>[0.]<br>[0.]]|
# |**W2 =**|[[-0.23364061 -0.31486371 -0.89285909]]|
# |**b2 =**|[[0.]]|
# 
# ***
# 
# ## 5 Implement forward_deep
# **1.5 points**
# 
# For a $L$-layer neural network, the whole forward propagation runs the previously defined `forward` with `func='relu'` $L-1$ times, and follows with `forward` with `func='sigmoid'` (see the figure below).
# 
# <img src='forward_deep.png' style="width: 1000px" />
# 
# Instructions:
# - `forward_deep` takes input data $X$ and `params` as arguments, and compute $L$ steps of forward propagation using the $W^{[l]}$ and $b^{[l]}$ stored in `params`.
# - It collects the `cache` from each layer and store them in a python list `caches`

# In[10]:


def forward_deep(X, params):
    """
    Args:
    X -- input data of shape (input_size, m)
    params -- output of init_params_deep
    
    Returns:
    AL -- the activation of the output layer
    caches -- a python list containing the cache object for each layer
    """
    caches = []
    A_prev = X
    L = len(params) // 2 # Because each layer has 2 parametes, this will give the number of layers
    
    # Forward propagation for the first L-1 relu layers
    for l in range(1, L):
        ### START YOUR CODE ###
        # Retrieve parameters
        W = params['W' + str(l)]
        b = params['b' + str(l)]
        
        # Call forward function, with 'relu'
        A, cache = forward(A_prev, W, b, func='relu')
        ### END YOUR CODE ###
        
        caches.append(cache)
        A_prev = A
    
    # Forward propagation for the last sigmoid output layer
    ### START YOUR CODE ###
    # Retrieve parameters
    W = params['W' + str(L)]
    b = params['b' + str(L)]
    
    # Call forward function, with 'sigmoid'
    AL, cache = forward(A_prev, W, b, func='sigmoid')
    ### END YOUR CODE ###
    caches.append(cache)
    
    return AL, caches


# In[11]:


# Evaluate Task 5
X, params = utils.forward_deep_testcase()

AL, caches = forward_deep(X, params)
print('AL.shape = ', AL.shape)
print('mean of AL =', np.mean(AL))
print('Length of caches =', len(caches))


# **Expected output**
# 
# |&nbsp;|&nbsp;|          
# |--|--|
# |**AL.shape =**|(1, 100)|
# |**mean of AL =**|0.4999999588605858|
# |**Length of caches =**|5|
# 
# ***
# 
# ## Decomposing backward block
# 
# The `backward` block of layer $l$ can be decomposed into two sub-modules as well: `activation_backward` and `linear_backward`.
# 
# - `activation_backward` takes $dA^{[l]}$ as input to compute $dZ^{[l]}$, and then passes it to `linear_backward`.
# - `linear_backward` takes $dZ^{[l]}$ to compute $dW^{[l]}$, $db^{[l]}$, and $dA^{[l-1]}$, and passes the latter to the previous layer.
# 
# Both modules need $\text{cache}^{[l]}$ to do the computation. $\text{cache}^{[l]} = (\text{linear_cache}^{[l]}, \text{activation_cache}^{[l]})$
# - `activation_backward` uses $\text{activation_cache}^{[l]})$
# - `linear_backward` uses $\text{linear_cache}^{[l]})$
# 
# See the figure below.
# 
# <img src='backward_decompose.png' style="width: 800px" />
# 
# ***
# 
# ## 6 Implement activation_backward
# **0.5 point**
# 
# The formula of computing $dZ^{[l]}$ is: $dZ^{[l]} = dA^{[l]} * g'(Z^{[l]})$
# - For sigmoid function, $g'(z) = g(z)(1-g(z))$, therefore, $g'(Z^{[l]}) = A^{[l]}(1 - A^{[l]})$
# - For ReLU function, $g'(z) = \begin{cases}1, & \text{if } Z>0.\\ 0, & \text{otherwise.} \end{cases}$
# 
# Instructions:
# - You only need to implement `sigmoid_backward`. `relu_backward` is implemented for you.
# - The `activation_cache` argument is just the `Z` you need.
# - `activation_backward` wraps around the above two functions.

# In[12]:


def sigmoid_backward(dA, activation_cache):
    """
    Args:
    dA -- gradient passed from next layer
    activation_cache -- 'Z',  which is stored during forward propagation
    
    Returns:
    dZ -- Gradient of the cost w.r.t. Z
    """
    Z = activation_cache
    
    ### START TODO ###
    A = utils.sigmoid(Z) # Compute A from Z
    dZ = dA * A * (1 - A) # Compute dZ
    ### END TODO ###
    
    assert (dZ.shape == Z.shape)
    
    return dZ

def relu_backward(dA, activation_cache):
    """
    Args:
    dA -- gradient passed from next layer
    activation_cache -- 'Z',  which is stored during forward propagation
    
    Returns:
    dZ -- Gradient of the cost w.r.t. Z
    """
    Z = activation_cache
    dZ = np.array(dA, copy=True)
    
    # Set dZ to 0 when Z <= 0. 
    dZ[Z <= 0] = 0
    
    assert (dZ.shape == Z.shape)
    
    return dZ

def activation_backward(dA, activation_cache, func):
    """
    Args:
    dA -- gradient passed from next layer
    activation_cache -- 'Z',  which is stored during forward propagation
    func -- 'sigmoid' or 'relu', indicates the activation function
    
    Returns:
    dZ -- Gradient of the cost w.r.t. Z
    """
    if func == 'sigmoid':
        dZ = sigmoid_backward(dA, activation_cache)
    elif func == 'relu':
        dZ = relu_backward(dA, activation_cache)
    
    return dZ


# In[13]:


# Evaluate Task 6
dA, activation_cache = utils.activation_backward_testcase()

dZ = activation_backward(dA, activation_cache, func='sigmoid')
print('With sigmoid, dZ =', dZ)
dZ = activation_backward(dA, activation_cache, func='relu')
print('With relu, dZ =', dZ)


# **Expected output**
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**With sigmoid, dZ =**|[[-0.10414453 -0.01044791 -0.49705693]<br>[ 0.3756869  -0.44831788 -0.15174916]<br>[ 0.10965887 -0.31131568 -0.21940518]<br>[-0.22586725  0.13561934  0.45300721]]|
# |**With relu, dZ =**|[[-0.41675785  0.         -2.1361961 ]<br>[ 0.          0.         -0.84174737]<br>[ 0.         -1.24528809  0.        ]<br>[ 0.          0.55145404  0.        ]]|
# 
# ***
# 
# ## 7 Implement linear_backward
# **1 point**
# 
# Use the following formulas:
# - $dW^{[l]} = \frac{1}{m}dZ^{[l]}A^{[l-1]T}$
# - $db^{[l]} = \frac{1}{m}\text{np.sum}(dZ, \text{axis}=1, \text{keepdims}=\text{True})$
# - $dA^{[l-1]} = W^{[l]T}dZ^{[l]}$

# In[14]:


def linear_backward(dZ, linear_cache):
    """
    Args:
    dZ -- Gradient passed from activation_backward
    linear_cache -- the tuple of (A_prev, W, b), stored during forward propagation

    Returns:
    dA_prev -- Gradient to pass to previous layer (layer l-1), same shape as A_prev
    dW -- Gradient of W (current layer l), same shape as W
    db -- Gradient of b (current layer l), same shape as b
    """
    A_prev, W, b = linear_cache # Retrieve cached variables
    m = A_prev.shape[1]
    
    ### START YOUR CODE ###
    dW = (1/m) * np.dot(dZ, A_prev.T)
    db = (1/m) * np.sum(dZ, axis=1, keepdims=True)
    dA_prev = np.dot(W.T, dZ)
    ### END YOUR CODE ###
    
    return dA_prev, dW, db


# In[15]:


# Evaluate Task 7
dZ, linear_cache = utils.linear_backward_testcase()

dA_prev, dW, db = linear_backward(dZ, linear_cache)
print('dA_prev =', dA_prev)
print('dW =', dW)
print('db =', db)


# **Expected output**
# 
# |&nbsp;|&nbsp;|          
# |--|--|
# |**dA_prev =**|[[-0.6916374   1.1105614   1.2886543 ]<br>[-0.54290127  1.38111692  0.61709641]]|
# |**dW =**|[[-0.36864801 -0.75350079]<br>[ 0.53977074 -0.64420274]<br>[ 0.28091023 -0.5063566 ]<br>[ 0.19379656  1.07490378]]|
# |**db =**|[[-0.86974026]<br>[-0.33163738]<br>[-0.60011963]<br>[ 0.64488481]]|
# 
# ***
# 
# ## 8 Impliment backward
# **1 point**
# 
# In `backward`, call `activation_backward` and `linear_backward` in order, and return gradients.

# In[16]:


def backward(dA, cache, func):
    """
    Args:
    dA -- gradient passed from next layer
    cache -- a tuple of (linear_cache, activation_cache)
    func -- 'sigmoid' or 'relu', indicates the activation function
    
    Returns:
    dA_prev -- Gradient to pass to previous layer (layer l-1)
    dW -- Gradient of W (current layer l)
    db -- Gradient of b (current layer l)
    """
    # Unpack cache
    linear_cache, activation_cache = cache
    
    ### START YOUR CODE ###
    dZ = activation_backward(dA, activation_cache, func)
    dA_prev, dW, db = linear_backward(dZ, linear_cache)
    ### END YOUR CODE ###
    
    return dA_prev, dW, db


# In[17]:


# Evaluate Task 8
dA, cache = utils.backward_testcase()

dA_prev, _, _ = backward(dA, cache, func='sigmoid')
print('With sigmoid, dA_prev =', dA_prev)
dA_prev, _, _ = backward(dA, cache, func='relu')
print('With relu, dA_prev =', dA_prev)


# **Expected output**
# 
# |&nbsp;|&nbsp;|          
# |--|--|
# |**With sigmoid, dA_prev =**|[[-0.14734295  0.27565994  0.2952049 ]<br>[-0.11479235  0.34582924  0.12920287]]|
# |**With relu, dA_prev =**|[[ 0.31168109 -0.50634887  2.33674401]<br>[-0.00376134  1.10107013  0.11239834]]|
# 
# ***
# 
# ## 9 Implement backward_deep
# **2 points**
# 
# Backward propagation starts from the output layer. 
# 
# The loss function for binary classification is: $L = -(Y\log(A^{[L]}) + (1-Y)\log(1 - A^{[L]}))$
# 
# Take its derivative w.r.t $A^{[L]}$ results in: $dA^{[L]} = - (\frac{Y}{A^{[L]}} - \frac{1-Y}{1-A^{[L]}})$.
# 
# Then you compute $dA^{[L-1]}$, $dW^{[L]}$, and $db^{[L]}$ by calling `backward` function with `func='sigmoid'`.
# - You store all the gradients to a python dict object `grads`.
# 
# After this step, for $l$ from $L-1$ to $1$, you compute $dA^{[l-1]}$, $dW^{[l]}$, and $db^{[l]}$ by calling `backward` function with `func='relu'`
# - In step $l$, when you need $dA^{[l]}$ as input, you can access it by `grads['dA' + str(l)]`.

# In[18]:


def backward_deep(AL, Y, caches):
    """
    Implement the backward propagation for the [LINEAR->RELU] * (L-1) -> LINEAR -> SIGMOID group
    
    Arguments:
    AL -- probability vector, output of the forward propagation (L_model_forward())
    Y -- true "label" vector (containing 0 if non-cat, 1 if cat)
    caches -- list of caches containing:
                every cache of linear_activation_forward() with "relu" (it's caches[l], for l in range(L-1) i.e l = 0...L-2)
                the cache of linear_activation_forward() with "sigmoid" (it's caches[L-1])
    
    Returns:
    grads -- A dictionary with the gradients
             grads["dA" + str(l)] = ... 
             grads["dW" + str(l)] = ...
             grads["db" + str(l)] = ... 
    """
    grads = {}
    L = len(caches) # the number of layers
    m = AL.shape[1]
    
    # Compute dAL
    ### START YOUR CODE ### 
    dAL = -(np.divide(Y, AL) - np.divide(1 - Y, 1 - AL))
    ### END YOUR CODE ###
    
    # Lth layer, Input: "AL, Y, caches". Outputs: "grads["dAL-1"], grads["dWL"], grads["dbL"]
    ### START YOUR CODE ### 
    current_cache = caches[L - 1: L]
    dA_prev_temp, dW_temp, db_temp = backward(dAL, current_cache[0], func='sigmoid')
    
    grads["dA" + str(L-1)] = dA_prev_temp
    grads["dW" + str(L)] = dW_temp
    grads["db" + str(L)] = db_temp
    ### END YOUR CODE ###
    
    # For layer L-1 to layer 1
    for l in reversed(range(L-1)):
        # Inputs: "grads["dA" + str(l + 2)], caches". Outputs: "grads["dA" + str(l + 1)] , grads["dW" + str(l + 1)] , grads["db" + str(l + 1)] 
        ### START YOUR CODE ###
        current_cache = caches[l: l + 1]
        dA_prev_temp, dW_temp, db_temp = backward(grads["dA" + str(l + 1)], current_cache[0], func='relu')
        
        grads["dA" + str(l)] = dA_prev_temp
        grads["dW" + str(l + 1)] = dW_temp
        grads["db" + str(l + 1)] = db_temp
        ### END YOUR CODE ###

    return grads


# In[19]:


# Evaluate Task 9
AL, Y, caches = utils.backward_deep_testcase()

grads = backward_deep(AL, Y, caches)

# print(grads)
print ("dW1 = "+ str(grads["dW1"]))
print ("db1 = "+ str(grads["db1"]))
print ("dA1 = "+ str(grads["dA1"]))


# **Expected output**
# 
# |&nbsp;|&nbsp;|          
# |--|--|
# |**dW1 =**|[[0.41010002 0.07807203 0.13798444 0.10502167]<br>[0.         0.         0.         0.        ]<br>[0.05283652 0.01005865 0.01777766 0.0135308 ]]|
# |**db1 =**|[[-0.22007063]<br>[ 0.        ]<br>[-0.02835349]]|
# |**dA1 =**|[[ 0.12913162 -0.44014127]<br>[-0.14175655  0.48317296]<br>[ 0.01663708 -0.05670698]]|
# 
# ***
