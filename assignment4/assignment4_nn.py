#!/usr/bin/env python
# coding: utf-8

# # CS596 Machine Learning 
# # Assignment 4: Shallow Neural Network
# 
# ### Due 11:59 pm, Friday, 10/25/2019
# 
# **Total points: 10**
# 
# In Assignment 4, you will implement a 2-layer shallow neural network model. 
# 
# We will use the model to conduct the same binary classification task as Assignment 3, i.e., classify two categories of the sign language dataset. 
# 
# The input size is the number of pixels in a image ($64\times 64$). The size of hidden layer is determined by a hyperparameter `n_h`, and the size of output layer is 1.

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
from utils import *
# import importlib
# importlib.reload(utils)

get_ipython().run_line_magic('matplotlib', 'inline')
np.random.seed(1)


# In[2]:


# Load data
### DO NOT CHANGE THE CODE BELOW ###
X_train, Y_train, X_test, Y_test = load_data()

print(X_train.shape)
print(Y_train.shape)
print(X_test.shape)
print(Y_test.shape)


# ### 1.1 Intialize parameters
# **1 point**
# 
# The parameters associated with the hidden layer are $W^{[1]}$ and $b^{[1]}$, and the parameters associated with the output layer are $W^{[2]}$ and $b^{[2]}$.
# 
# We use **tanh** as acitivation function for hidden layer, and **sigmoid** for output layer.
# 
# **Instructions:**
# - Initialize parameters randomly
# - Use `np.random.randn((size_out, size_in))*0.01` to initialize $W^{[l]}$, in which `size_out` is the output size of current layer, and `size_in` is the input size from previous layer. 
# - Use `np.zeros()` to initialize $b^{[l]}$

# In[3]:


def init_params(n_i, n_h, n_o):
    """
    Args:
    n_i -- size of input layer
    n_h -- size of hidden layer
    n_o -- size of output layer
    
    Return:
    params -- a dict object containing all parameters:
        W1 -- weight matrix of layer 1
        b1 -- bias vector of layer 1
        W2 -- weight matrix of layer 2
        b2 -- bias vector of layer 2
    """
    np.random.seed(2) # DO NOT change this line! 
    
    ### START YOUT CODE ###
    W1 = np.random.randn(n_h,n_i) * 0.01
    b1 = np.zeros((n_h,1))
    W2 = np.random.randn(n_o,n_h) * 0.01
    b2 = np.zeros((n_o,1))
    ### END YOUT CODE ###
    
    params = {'W1': W1, 'b1': b1, 'W2': W2, 'b2': b2}
    
    return params


# In[4]:


# Evaluate Task 1.1
### DO NOT CHANGE THE CODE BELOW ###
ps = init_params(3, 4, 1)
print('W1 =', ps['W1'])
print('b1 =' ,ps['b1'])
print('W2 =', ps['W2'])
print('b2 =', ps['b2'])


# **Expected output**
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**W1 =**|[[-0.00416758 -0.00056267 -0.02136196] <br>[ 0.01640271 -0.01793436 -0.00841747] <br> [ 0.00502881 -0.01245288 -0.01057952]<br>[-0.00909008  0.00551454  0.02292208]]|
# |**b1 =**|[[0.]<br>[0.]<br>[0.]<br>[0.]]|
# |**W2 =**|[[ 0.00041539 -0.01117925  0.00539058 -0.0059616 ]]|
# |**b2 =**|[[0.]]|
# 
# ***
# 
# ### 1.2 Forward propagation
# 
# **2 points**
# 
# Use the following fomulas to implement forward propagation:
# - $Z^{[1]} = W^{[1]}X + b^{[1]}$
# - $A^{[1]} = tanh(Z^{[1]})$ --> use `np.tanh` function
# - $Z^{[2]} = W^{[2]}A^{[1]} + b^{[2]}$
# - $A^{[2]} = \sigma(Z^{[2]})$ --> directly use the `sigmoid` function provided in `utils` package

# In[5]:


def forward_prop(X, params):
    """
    Args:
    X -- input data of shape (n_in, m)
    params -- a python dict object containing all parameters (output of init_params)
    
    Return:
    A2 -- the activation of the output layer
    cache -- a python dict containing all intermediate values for later use in backprop
             i.e., 'Z1', 'A1', 'Z2', 'A2'
    """
    m = X.shape[1]
    
    # Retrieve parameters
    ### START YOUR CODE ###
    W1 = params["W1"]
    b1 = params["b1"]
    W2 = params["W2"]
    b2 = params["b2"]
    ### END YOUT CODE ###
    
    # Implement forward propagation
    ### START YOUR CODE ###
    Z1 = np.dot(W1,X) + b1
    A1 = np.tanh(Z1)
    Z2 = np.dot(W2,A1) + b2
    A2 = sigmoid(Z2)
    ### END YOUR CODE ###
    
    assert A1.shape[1] == m
    assert A2.shape[1] == m
    
    cache = {'Z1': Z1, 'A1': A1, 'Z2': Z2, 'A2': A2}
    
    return A2, cache


# In[6]:


# Evaluate Task 1.2
### DO NOT CHANGE THE CODE BELOW ###
X_tmp, params_tmp = forwardprop_testcase()

A2, cache = forward_prop(X_tmp, params_tmp)
print('mean(Z1) =', np.mean(cache['Z1']))
print('mean(A1) =', np.mean(cache['A1']))
print('mean(Z2) =', np.mean(cache['Z2']))
print('mean(A2) =', np.mean(cache['A2']))


# **Expected output**
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**mean(Z1) =**|0.13208981347443063|
# |**mean(A1) =**|-0.01294750224234301|
# |**mean(Z2) =**|-0.028697749001905536|
# |**mean(A2) =**|0.5329353691451202|
# 
# ***
# 
# ### 1.3 Backward propagation
# **3 points**
# 
# Use the following formulas to implement backward propagation:
# - $dZ^{[2]} = A^{[2]} - Y$
# - $dW^{[2]} = \frac{1}{m}dZ^{[2]}A^{[1]T}$ --> $m$ is the number of examples
# - $db^{[2]} = \frac{1}{m}$ np.sum( $dZ^{[2]}$, axis=1, keepdims=True)
# - $dA^{[1]} = W^{[2]T}dZ^{[2]}$
# - $dZ^{[1]} = dA^{[1]}*g'(Z^{[1]})$
#     - Here $*$ denotes element-wise multiply
#     - $g(z)$ is the tanh function, therefore its derivative $g'(Z^{[1]}) = 1 - (g(Z^{[1]}))^2 = 1 - (A^{[1]})^2$
# - $dW^{[1]} = \frac{1}{m} dZ^{[1]}X^T$
# - $db^{[1]} = \frac{1}{m}$ np.sum( $dZ^{[1]}$, axis=1, keepdims=True)

# In[7]:


def backward_prop(X, Y, params, cache):
    """
    Args:
    X -- input data of shape (n_in, m)
    Y -- input label of shape (1, m)
    params -- a python dict containing all parameters
    cache -- a python dict containing 'Z1', 'A1', 'Z2' and 'A2' (output of forward_prop)
    
    Return:
    grads -- a python dict containing the gradients w.r.t. all parameters,
             i.e., dW1, db1, dW2, db2
    """
    m = X.shape[1]
    
    # Retrieve parameters
    ### START YOUR CODE ###
    W1 = params["W1"]
    W2 = params["W2"]
    ### END YOUR CODE ###
    
    # Retrive intermediate values stored in cache
    ### START YOUR CODE ###
    A1 = cache["A1"]
    A2 = cache["A2"]
    ### END YOUR CODE ###
    
    # Implement backprop
    ### START YOUR CODE ###
    dZ2 = A2 - Y
    dW2 = (1/m) * np.dot(dZ2,A1.T)
    db2 = (1/m) * np.sum(dZ2, axis=1, keepdims=True)
    dA1 = np.dot(W2.T,dZ2)
    dZ1 = dA1 * (1 - np.power(A1, 2))
    dW1 = (1/m) * np.dot(dZ1, X.T)
    db1 = (1/m) * np.sum(dZ1, axis=1, keepdims=True)
    ### END YOUR CODE ###
    
    grads = {'dW1': dW1, 'db1': db1, 'dW2': dW2, 'db2': db2}
    
    return grads


# In[8]:


# Evaluate Task 1.3
### DO NOT CHANGE THE CODE BELOW ###
X_tmp, Y_tmp, params_tmp, cache_tmp = backprop_testcase()

grads = backward_prop(X_tmp, Y_tmp, params_tmp, cache_tmp)
print('mean(dW1)', np.mean(grads['dW1']))
print('mean(db1)', np.mean(grads['db1']))
print('mean(dW2)', np.mean(grads['dW2']))
print('mean(db2)', np.mean(grads['db2']))


# **Expected output**
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**mean(dW1) =**|-0.039558211695590706|
# |**mean(db1) =**|0.001467912358907287|
# |**mean(dW2) =**|0.1250823230639841|
# |**mean(db2) =**|0.13293536800000003|
# 
# ***
# 
# ### 1.4 Update parameters
# **1 point**
# 
# Update $W^{[1]}, b^{[1]}, W^{[2]}, b^{[2]}$ accordingly:
# - $W^{[1]} = W^{[1]} - \alpha\ dW^{[1]}$
# - $b^{[1]} = b^{[1]} - \alpha\ db^{[1]}$
# - $W^{[2]} = W^{[2]} - \alpha\ dW^{[2]}$
# - $b^{[2]} = b^{[2]} - \alpha\ db^{[2]}$

# In[9]:


def update_params(params, grads, alpha):
    """
    Args:
    params -- a python dict containing all parameters
    grads -- a python dict containing the gradients w.r.t. all parameters (output of backward_prop)
    alpha -- learning rate
    
    Return:
    params -- a python dict containing all updated parameters
    """
    # Retrieve parameters
    ### START YOUR CODE ###
    W1 = params["W1"]
    b1 = params["b1"]
    W2 = params["W2"]
    b2 = params["b2"]
    ### END YOUR CODE ###
    
    # Retrieve gradients
    ### START YOUR CODE ###
    dW1 = grads["dW1"]
    db1 = grads["db1"]
    dW2 = grads["dW2"]
    db2 = grads["db2"]
    ### END YOUR CODE ###
    
    # Update each parameter
    ### START YOUR CODE ###
    W1 = W1 - alpha * dW1
    b1 = b1 - alpha * db1
    W2 = W2 - alpha * dW2
    b2 = b2 - alpha * db2
    ### END YOUR CODE ###
    
    params = {'W1': W1, 'b1': b1, 'W2': W2, 'b2': b2}
    
    return params


# In[10]:


# Evaluate Task 1.4
### DO NOT CHANGE THE CODE BELOW ###
params_tmp, grads_tmp = update_params_testcase()

params = update_params(params_tmp, grads_tmp, 0.01)
print('W1 =', params['W1'])
print('b1 =', params['b1'])
print('W2 =', params['W2'])
print('b2 =', params['b2'])


# **Expected output**
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**W1 =**|[[-1.9959083  -1.06667372 -0.75475925]<br>[ 0.29418098 -0.98950432 -1.22186039]<br>[-0.27355221 -1.6082775   0.11104952]<br>[-0.14229044  1.07321512  0.4208789 ]<br>[ 0.59648267  0.86048013 -1.60750032]]|
# |**b1 =**|[[-0.00062513]<br>[ 0.00046574]<br>[ 0.00069734]<br>[-0.0002741 ]<br>[-0.00033725]]|
# |**W2 =**|[[ 1.08049444 -0.25269532  1.0989616   0.20063139  1.45914531]]|
# |**b2 =**|[[-0.00132935]]|
# 
# ***
# 
# ### 1.5 Integrated model
# **1.5 points**
# 
# Integrate `init_params`, `forward_prop`, `backward_prop` and `update_params` into one model.

# In[11]:


def nn_model(X, Y, n_h, num_iters=10000, alpha=0.01, verbose=False):
    """
    Args:
    X -- training data of shape (n_in, m)
    Y -- training label of shape (1, m)
    n_h -- size of hidden layer
    num_iters -- number of iterations for gradient descent
    verbose -- print cost every 1000 steps
    
    Return:
    params -- parameters learned by the model. Use these to make predictions on new data
    """
    np.random.seed(3)
    m = X.shape[1]
    n_in = X.shape[0]
    n_out = 1
    
    # Initialize parameters and retrieve them
    params = init_params(n_in, n_h, n_out)
    W1 = params['W1']
    b1 = params['b1']
    W2 = params['W2']
    b2 = params['b2']
    
    # Gradient descent loop
    for i in range(num_iters):
        ### START YOUR CODE ###
        # Forward propagation
        A2, cache = forward_prop(X, params)
        
        # Backward propagation
        grads = backward_prop(X, Y, params, cache)
        
        # Update parameters
        params = update_params(params, grads, alpha)
        ### END YOUR CODE ###
        
        # Compute cost
        cost = -(1/m) * np.sum(np.log(A2) * Y + np.log(1 - A2) * (1 - Y))
        
        # Print cost
        if i % 1000 == 0 and verbose:
            print('Cost after iter {}: {}'.format(i, cost))
    
    return params


# In[12]:


# Evaluate Task 1.5
### DO NOT CHANGE THE CODE BELOW ###
X_tmp, Y_tmp = nn_model_testcase()

params_tmp = nn_model(X_tmp, Y_tmp, n_h=5, num_iters=5000, alpha=0.01)
print('W1 =', params_tmp['W1'])
print('b1 =', params_tmp['b1'])
print('W2 =', params_tmp['W2'])
print('b2 =', params_tmp['b2'])


# **Expected output**
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**W1 =**|[[ 0.728558   -0.60417473 -0.24274211]<br>[ 0.88560809 -0.67439594 -0.3043778 ]<br>[-0.20781606 -0.59195986 -0.27344463]<br>[-0.51914662  0.61152697  0.17713134]<br>[ 0.00864946 -0.25198231 -0.1411225 ]]|
# |**b1 =**|[[ 0.29073376]<br>[ 0.29189656]<br>[ 0.28876041]<br>[-0.32656432]<br>[ 0.09711243]]|
# |**W2 =**|[[-1.25312586 -1.40689892 -0.69967068  1.13815825 -0.31472553]]|
# |**b2 =**|[[-0.80345148]]|
# 
# ***
# 
# ### 1.6 Predict
# **1 point**
# 
# Use the learned parameters to make predictions on new data. 
# - Compute $A^{[2]}$ by calling `forward_prop`. Note that the `cache` returned will not be used in making predictions.
# - Convert $A^{[2]}$ into a vector of 0 and 1.

# In[13]:


def predict(X, params):
    """
    Args:
    X -- input data of shape (n_in, m)
    params -- a python dict containing the learned parameters
    
    Return:
    pred -- predictions of model on X, a vector of 0s and 1s
    """
    ### START YOUR CODE ###
    A2, _ = forward_prop(X, params)
    pred = np.where(A2 > 0.5, 1, 0) # Convert to a vector of 0 and 1.
    ### END YOUR CODE ###
    
    return pred


# In[14]:


# Evaluate Task 1.6
# NOTE: the X_tmp and params_tmp are the ones generated in evaluating Task 1.5 (two cells above)
### DO NOT CHANGE THE CODE BELOW ###

pred = predict(X_tmp, params_tmp)
print('predictions = ', pred)


# **Expected output**
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**predictions =**|[[0. 1. 0. 0. 1.]]|
# 
# ***
# 
# ### 1.7 Train and evaluate
# 
# **0.5 point**
# 
# Train the neural network model on X_train and Y_train, and evaluate it on X_test and Y_test.
# 
# You can use the code from HA3 to compute the accuracy of your predictions.

# In[15]:


# Train the model on X_train and Y_train, and print cost
# DO NOT change the hyperparameters, so that your output matches the expected one.
params = nn_model(X_train, Y_train, n_h = 10, num_iters=10000, verbose=True)

# Make predictions on X_test
predictions = predict(X_test, params)

# Compute accuracy
### START YOUR CODE ###
acc = float((np.dot(Y_test, predictions.T) + np.dot(1 - Y_test, 1 - predictions.T))) / float(Y_test.size)
### END YOUR CODE ###
print('Accuracy = {0:.2f}%'.format(acc * 100))


# **Expected output**
# 
# |&nbsp;|&nbsp; |          
# |--|--|
# |**Cost after iter 0:**|0.6931077265775999|
# |**Cost after iter 1000:**|0.2482306581297105|
# |**Cost after iter 2000:**|0.05471507033938196|
# |**Cost after iter 3000:**|0.024326463013581715|
# |**Cost after iter 4000:**|0.014595754197204438|
# |**Cost after iter 5000:**|0.010131520880123288|
# |**Cost after iter 6000:**|0.00764463387660483|
# |**Cost after iter 7000:**|0.0060842030981856435|
# |**Cost after iter 8000:**|0.005023835721723831|
# |**Cost after iter 9000:**|0.0042610856757679645|
# |**Accuracy =** |95.20%|
# 
# ***
