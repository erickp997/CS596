#!/usr/bin/env python
# coding: utf-8

# # CS596 Machine Learning 
# # Assignment 1: Python and numpy basics
# 
# ### Due 11:59 pm, Friday, 9/13/2019
# 
# **Total: 10 points**
# 
# In this assignment, you will need to solve a series of programming questions about Python and numpy. You are encourageed to look up the official documents (Python: https://docs.python.org/3/; numpy: https://docs.scipy.org/doc/numpy-1.17.0/reference/) and Google to help you.
# 
# Your goal is to run all the cells below one by one from top to bottom. Before you run some task cells, you need to complete the missing code (indicated by the **None**s) in them. 
# 
# For each **task** cell that requires your completion, you should run it to check if your code is correct.
# The cell output should be *exactly* the same as the "expected output" that follows.
# 
# ---
# 
# ## Import packages

# In[1]:


import numpy as np


# ## Task 1: Python list operations
# 
# **2 points**
# 
# Task descriptions are in the comments.
# 
# *Hint*: Use the technique called *list comprehensions*, you can write compact code on list operations. Link: https://docs.python.org/3/tutorial/datastructures.html#list-comprehensions

# In[2]:


#### START YOUR CODE ####
# Create a list of 50 even integer numbers, ranging from 2 to 100, e.g., [2, 4, 6, ..., 100]
# Hint: use the range() function
evens = [x for x in range(2, 101) if x % 2 == 0]

# Find all the numbers that can be divided by number 3 with 0 remainder, and store them in a list
# E.g., [6, 12, ..., 96]
# Hint: use list comprehensions to write just one line of code
divisibles = [x for x in range(2, 101) if x % 3 == 0 and x % 2 == 0]
#### END YOUR CODE ####


#### DO NOT CHANGE THE CODE BELOW ####
assert isinstance(evens, list)
print('len(evens) = {}'.format(len(evens)))
print('evens[20:30] = {}'.format(evens[20:30]))

assert(isinstance(divisibles, list))
print('len(divisibles) = {}'.format(len(divisibles)))
print('divibles = {}'.format(divisibles))


# ### Expected output
# &nbsp;|&nbsp;
# --|--
# len(evens) = | 50
# evens[20:30] = | [42, 44, 46, 48, 50, 52, 54, 56, 58, 60]
# len(divisibles) = |16
# divibles = |[6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66, 72, 78, 84, 90, 96]
# 
# ---
# 
# ## Task 2: Vectorized comparison in numpy
# **3 points**
# 
# Task descriptions are in the comments.

# In[3]:


np.random.seed(0)

#### START YOUR CODE ####
# Create a 3 by 3 numpy array with random float numbers sampled from a normal distribution (mean=0, std=1)
a = np.random.randn(3,3)

# Find how many elements are greater than or equal to 0.5 in array a. 
# Hint: numpy array can be directly compared with constant numbers; In Python, True == 1, and False == 0
count = len(np.argwhere(a > 0.5))

# Find the indices of all elements in array a that are greater than or equal to 0.5
# Hint: use np.argwhere()
indices = (np.argwhere(a > 0.5)).tolist()
#### END YOUR CODE ####


#### DO NOT CHANGE THE CODE BELOW ####
assert isinstance(a, np.ndarray)
print('a = {}'.format(a))
print('count = {}'.format(count))
assert isinstance(indices, list)
print('indices = {}'.format(indices))


# ### Expected output
# &nbsp;|&nbsp;
# --|--
# a = |[[ 1.76405235  0.40015721  0.97873798] <br>[ 2.2408932   1.86755799 -0.97727788] <br>[ 0.95008842 -0.15135721 -0.10321885]]
# count = |5
# indices = |[[0, 0], [0, 2], [1, 0], [1, 1], [2, 0]]
# 
# ---
# 
# 
# ## Task 3: mean() and argmax() in numpy
# **3 points**
# 
# Task descriptions are in the comments.

# In[5]:


b = np.arange(20).reshape((4,5))

#### START YOUR CODE ####
# Compute the mean value for each column in the following 4 by 5 array
# Hint: use np.mean() with the argument axis set properly
column_means = np.mean(b, 1)

# Find the max element for each column, and their column indices
# Hint: use np.amax() and np.argmax()
row_max = np.amax(b, 1)
row_max_indices = np.argmax(b, 1)
#### END YOUR CODE ####


#### DO NOT CHANGE THE CODE BELOW ####
# print(b)
assert isinstance(column_means, np.ndarray)
print('column means = {}'.format(column_means))
print('max elements for rows = {}'.format(row_max))
print('indices of max elements for rows = {}'.format(row_max_indices))


# ### Expected output
# &nbsp;|&nbsp;
# --|--
# column means = |[ 2.  7. 12. 17.]
# max elements for rows = |[ 4  9 14 19]
# indices of max elements for rows = |[4 4 4 4]
# 
# ---
# 
# ## Task 4: Basic linear algebra in numpy
# **2 points**
# 
# Task descriptions are in the comments.
# 
# *Reference*: https://docs.scipy.org/doc/numpy/reference/routines.linalg.html

# In[10]:


m = np.array([[1.,2.], [3.,4.]])

#### START YOUR CODE ####
# Compute the inverse of matrix m
m_inv = np.linalg.inv(m)

# Compute the eigen values and eigen vectors of m
w, v = np.linalg.eig(m)
#### END YOUR CODE ####

#### DO NOT CHANGE THE CODE BELOW ####
print('m_inv = ', m_inv)
print('m_inv is close to np.eye(2): ', np.allclose(m.dot(m_inv), np.eye(2)))
print('eigen values w = ', w)
print('eigen vectors v = ', v)
print('mv = wv: ', np.allclose(m.dot(v), w*v))


# ### Expected output
# &nbsp;|&nbsp;
# --|--
# m_inv = |[[-2.   1. ]<br>[ 1.5 -0.5]]
# m_inv is close to np.eye(2): | True
# w = | [-0.37228132  5.37228132]
# v = | [[-0.82456484 -0.41597356]<br>[ 0.56576746 -0.90937671]]
# mv = wv: | True
