#!/usr/bin/env python
# coding: utf-8

# # CS596 Machine Learning 
# # Assignment 7 (Part 1): Basic Recurrent Neural Network
# 
# ### Due 11:59 pm, Wednesday, 11/27/2019
# 
# **Total points: 4**
# 
# In Assignment 7 (Part 1), you will implement the forward pass for a basic recurrent neural network.

# In[1]:


import numpy as np
from rnn1_utils import *


# ## Task 7.1 Basic RNN cell
# 
# **1.5 points**
# 
# The following figure describes the operations for a single time-step of an RNN cell. 
# 
# <img src="rnn_step_forward.png" style="width:500px;height:220px;">
# <caption><center> **Figure 2**: Basic RNN cell. Takes as input $x^{\langle t \rangle}$ (current input) and $a^{\langle t - 1\rangle}$ (previous hidden state containing information from the past), and outputs $a^{\langle t \rangle}$ which is given to the next RNN cell and also used to predict $y^{\langle t \rangle}$ </center></caption>
# 
# **Instructions**:
# 1. Compute the hidden state with tanh activation: $a^{\langle t \rangle} = \tanh(W_{aa} a^{\langle t-1 \rangle} + W_{ax} x^{\langle t \rangle} + b_a)$.
# 2. Using your new hidden state $a^{\langle t \rangle}$, compute the prediction $\hat{y}^{\langle t \rangle} = softmax(W_{ya} a^{\langle t \rangle} + b_y)$. We provided you a function: `softmax`.
# 3. Store $(a^{\langle t \rangle}, a^{\langle t-1 \rangle}, x^{\langle t \rangle}, parameters)$ in `cache`
# 4. Return $a^{\langle t \rangle}$ , $y^{\langle t \rangle}$ and `cache`
# 
# We will vectorize over $m$ examples. Thus, $x^{\langle t \rangle}$ will have dimension $(n_x,m)$, and $a^{\langle t \rangle}$ will have dimension $(n_a,m)$. 

# In[2]:


def rnn_cell_forward(xt, a_prev, params):
    """
    Implements a single forward step of the RNN-cell
    Args:
    xt --  Input data at timestep "t", numpy array of shape (n_x, m).
    a_prev -- Hidden state at timestep "t-1", numpy array of shape (n_a, m)
    params -- A Python dictionary object containing:
                        Wax -- Weight matrix multiplying the input, numpy array of shape (n_a, n_x)
                        Waa -- Weight matrix multiplying the hidden state, numpy array of shape (n_a, n_a)
                        Wya -- Weight matrix relating the hidden-state to the output, numpy array of shape (n_y, n_a)
                        ba --  Bias, numpy array of shape (n_a, 1)
                        by -- Bias relating the hidden-state to the output, numpy array of shape (n_y, 1)
    Returns:
    a_next -- next hidden state, of shape (n_a, m)
    yt_pred -- prediction at timestep "t", numpy array of shape (n_y, m)
    cache -- tuple of values needed for the backward pass, contains (a_next, a_prev, xt, parameters)
    """
    
    # Retrieve parameters 
    Wax = params["Wax"]
    Waa = params["Waa"]
    Wya = params["Wya"]
    ba = params["ba"]
    by = params["by"]
    
    ### START YOUR CODE ###
    # compute next activation state using the formula given above
    a_next = np.tanh(np.dot(Waa, a_prev) + np.dot(Wax, xt) + ba)
    
    # compute output of the current cell using the formula given above
    yt_pred = softmax(np.dot(Wya, a_next) + by)
    ### END YOUR CODE ###
    
    # store values you need for backward propagation in cache
    cache = (a_next, a_prev, xt, parameters)
    
    return a_next, yt_pred, cache


# In[3]:


# Evaluate Task 7.1
np.random.seed(1)
xt = np.random.randn(3,10)
a_prev = np.random.randn(5,10)
Waa = np.random.randn(5,5)
Wax = np.random.randn(5,3)
Wya = np.random.randn(2,5)
ba = np.random.randn(5,1)
by = np.random.randn(2,1)
parameters = {"Waa": Waa, "Wax": Wax, "Wya": Wya, "ba": ba, "by": by}

a_next, yt_pred, cache = rnn_cell_forward(xt, a_prev, parameters)
print("a_next[4] = ", a_next[4])
print("a_next.shape = ", a_next.shape)
print("yt_pred[1] =", yt_pred[1])
print("yt_pred.shape = ", yt_pred.shape)


# **Expected Output**: 
# 
# <table>
#     <tr>
#         <td>
#             a_next[4]:
#         </td>
#         <td>
#            [ 0.59584544  0.18141802  0.61311866  0.99808218  0.85016201  0.99980978
#  -0.18887155  0.99815551  0.6531151   0.82872037]
#         </td>
#     </tr>
#         <tr>
#         <td>
#             a_next.shape:
#         </td>
#         <td>
#            (5, 10)
#         </td>
#     </tr>
#         <tr>
#         <td>
#             yt[1]:
#         </td>
#         <td>
#            [ 0.9888161   0.01682021  0.21140899  0.36817467  0.98988387  0.88945212
#   0.36920224  0.9966312   0.9982559   0.17746526]
#         </td>
#     </tr>
#         <tr>
#         <td>
#             yt.shape:
#         </td>
#         <td>
#            (2, 10)
#         </td>
#     </tr>
# 
# </table>

# ---
# 
# ## Task 7.2 Forward pass for basic RNN model
# 
# **2.5 points**
# 
# Implement RNN as the repetition of the `rnn_cell_forward()` function. 
# 
# If the input sequence is carried over 10 time steps, then you need to copy the RNN cell 10 times. Each cell takes as input the hidden state from the previous cell ($a^{\langle t-1 \rangle}$) and the current time-step's input data ($x^{\langle t \rangle}$). It outputs a hidden state ($a^{\langle t \rangle}$) and a prediction ($y^{\langle t \rangle}$) for the current time-step.
# 
# 
# <img src="rnn1.png" style="width:800px;height:300px;">
# <caption><center> **Figure 2**: Basic RNN. The input sequence $x = (x^{\langle 1 \rangle}, x^{\langle 2 \rangle}, ..., x^{\langle T_x \rangle})$  is carried over $T_x$ time steps. The network outputs $y = (y^{\langle 1 \rangle}, y^{\langle 2 \rangle}, ..., y^{\langle T_x \rangle})$. </center></caption>
# 
# 
# **Instructions**:
# 1. Create a vector of zeros ($a$) that will store all the hidden states computed by the RNN.
# 2. Initialize the `a_next` hidden state as $a_0$ (initial hidden state).
# 3. Start looping over each time step, your incremental index is $t$ :
#     - Update the `a_next` hidden state and the cache by calling `rnn_cell_forward()`
#     - Store the `a_next` hidden state in $a$ ($t^{th}$ position) 
#     - Store the prediction in y
#     - Add the cache to the list of caches
# 4. Return $a$, $y$ and caches

# In[4]:


def rnn_forward(x, a0, params):
    """
    Implement the forward propagation of the recurrent neural network 
    Args:
        x -- Input data for every time-step, of shape (n_x, m, T_x).
        a0 -- Initial hidden state, of shape (n_a, m)
        params -- python dictionary containing:
                Waa -- Weight matrix multiplying the hidden state, numpy array of shape (n_a, n_a)
                Wax -- Weight matrix multiplying the input, numpy array of shape (n_a, n_x)
                Wya -- Weight matrix relating the hidden-state to the output, numpy array of shape (n_y, n_a)
                ba --  Bias numpy array of shape (n_a, 1)
                by -- Bias relating the hidden-state to the output, numpy array of shape (n_y, 1)

    Returns:
        a -- Hidden states for every time-step, numpy array of shape (n_a, m, T_x)
        y_pred -- Predictions for every time-step, numpy array of shape (n_y, m, T_x)
        caches -- tuple of values needed for the backward pass, contains (list of caches, x)
    """
    
    # Initialize the list of all caches
    caches = []
    
    # Retrieve dimensions
    n_x, m, T_x = x.shape
    n_y, n_a = params["Wya"].shape
    
    
    ### START YOUR CODE ###
    
    # initialize "a" and "y" with zeros
    # Hint: use np.zeros(); the shape of "a" is (n_a, m, T_x)
    # the shape of "y_pred"  is (n_y, m, T_x)
    a = np.zeros([n_a, m, T_x])
    y_pred = np.zeros([n_y, m, T_x])
    
    # Initialize a_next with a0
    a_next = a0
    
    # loop over all time-steps
    for t in range(T_x):
        # Update next hidden state, compute the prediction, get the cache
        # Hint: the first argument of rnn_cell_forward() is the t_th element of x along the 3rd dimension
        a_next, yt_pred, cache = rnn_cell_forward(x[:,:,t], a_next, parameters)
        
        # Save the value of the new "a_next" hidden state in "a"
        a[:,:,t] = a_next
        
        # Save the current prediction in y_pred
        y_pred[:,:,t] = yt_pred
        
        # Append "cache" to "caches"
        caches.append(cache)
        
    ### END YOUR CODE ###
    
    # store values needed for backward propagation in cache
    caches = (caches, x)
    
    return a, y_pred, caches


# In[5]:


# Evaluate Task 7.2
np.random.seed(1)
x = np.random.randn(3,10,4)
a0 = np.random.randn(5,10)
Waa = np.random.randn(5,5)
Wax = np.random.randn(5,3)
Wya = np.random.randn(2,5)
ba = np.random.randn(5,1)
by = np.random.randn(2,1)
parameters = {"Waa": Waa, "Wax": Wax, "Wya": Wya, "ba": ba, "by": by}

a, y_pred, caches = rnn_forward(x, a0, parameters)
print("a[4][1] = ", a[4][1])
print("a.shape = ", a.shape)
print("y_pred[1][3] =", y_pred[1][3])
print("y_pred.shape = ", y_pred.shape)
print("caches[1][1][3] =", caches[1][1][3])
print("len(caches) = ", len(caches))


# **Expected Output**:
# 
# |&nbsp;|&nbsp;|
# |--|--|
# |**a[4][1]**:| [-0.99999375  0.77911235 -0.99861469 -0.99833267]|
# |**a.shape**:| (5, 10, 4) |
# |**y[1][3]**:| [ 0.79560373  0.86224861  0.11118257  0.81515947] |
# |**y.shape**:| (2, 10, 4) |
# |**cache[1][1][3]**:| [-1.1425182  -0.34934272 -0.20889423  0.58662319]|
# |**len(cache)**:| 2 |
# 
# ---
# 
# Congratulations! You've successfully built the forward propagation of a recurrent neural network from scratch.
